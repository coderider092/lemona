﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Lemona.Core.Startup))]
namespace Lemona.Core
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
