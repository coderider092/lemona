﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Lemona.Core
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet<ConversationRoom> Rooms { get; set; }
        public DbSet<Advert> Adverts { get; set; }
    }

    public class User
    {
        [Key]
        public string UserName { get; set; }
        public DateTime BirthDate { get; set; }
        public byte[] Image { get; set; }
        public ICollection<Connection> Connections { get; set; }
        public virtual ICollection<ConversationRoom> Rooms { get; set; }
        public virtual ICollection<Advert> Adverts { get; set; }
    }

    public class Connection
    {
        public string ConnectionID { get; set; }
        public string UserAgent { get; set; }
        public bool Connected { get; set; }
    }

    public class ConversationRoom
    {
        [Key]
        public string RoomName { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<string> Messages { get; set; }
    }

    public class Advert
    {
        public string AdvertName { get; set; }
        public string Text;
    }
}