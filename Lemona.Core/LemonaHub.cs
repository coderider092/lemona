﻿using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace Lemona.Core
{
    [Authorize]
    public class LemonaHub : Hub
    {
        public override async Task OnConnected()
        {
            using (var context = new UserContext())
            {
                var user = context.Users.Include(u => u.Rooms).SingleOrDefault(u => u.UserName == Context.User.Identity.Name);
                if (user == null)
                {
                    context.Users.Add(new User() { UserName = Context.User.Identity.Name });
                    await context.SaveChangesAsync();
                }
                else
                {
                    await Task.WhenAll(user.Rooms.Select(room => Task.Run(() => Groups.Add(Context.ConnectionId, room.RoomName))));
                }
            }
            await base.OnConnected();
        }

        public async Task JoinRoomAsync(string roomName)
        {
            using (var context = new UserContext())
            {
                var room = context.Rooms.Find(roomName);
                if (room != null)
                {
                    var user = new User() { UserName = Context.User.Identity.Name };
                    context.Users.Attach(user);

                    room.Users.Add(user);
                    await context.SaveChangesAsync();

                    await Groups.Add(Context.ConnectionId, roomName);
                    Clients.OthersInGroup(roomName).addToRoom(Context.User.Identity.Name, roomName);
                }
            }
        }

        public async Task LeaveRoomAsync(string roomName)
        {
            using (var context = new UserContext())
            {
                var room = context.Rooms.Find(roomName);
                if (room != null)
                {
                    var user = new User() { UserName = Context.User.Identity.Name };
                    context.Users.Attach(user);

                    room.Users.Remove(user);
                    await context.SaveChangesAsync();

                    Clients.OthersInGroup(roomName).removeFromRoom(Context.User.Identity.Name);
                    await Groups.Remove(Context.ConnectionId, roomName);
                }
            }
        }

        public async Task SendMsgAsync(string roomName, string messege)
        {
            using (var context = new UserContext())
            {
                var room = context.Rooms.Find(roomName);
                if (room != null)
                {
                    room.Messages.Add(messege);
                    await context.SaveChangesAsync();
                    Clients.OthersInGroup(roomName).addChatMsg(Context.User.Identity.Name, roomName, messege);
                }
            }
        }
    }
}